# PAT_System_repo
('o')ｷﾞｬｧｧｧｧｧｧｧｧｧｧｧｧｧｧｧｧｧｧwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
(((（ ´◔ ω◔`）)))ほおおおおおおおおおおおおおおおおおおおおおおおｗｗｗｗｗｗｗｗｗｗｗ
ｧｱﾞ～～～:((: ◜◡‾:)):～～～？？！！

## 各星系解説(てけとう)

### 1K
サイズ1000で資源の多い惑星が1つだけ。地形とかそういうの一切なし。

### 2K
サイズ2000で資源の多い惑星が1つだけ。地形とかそういうの一切なし。
きわめて重い。絶望的に広い。

### 4U
ふぉーゆーｗｗｗｗｗｗｗｗｗｗｗｗｗｗｗｗｗｗｗｗｗｗｗｗｗｗｗｗ
惑星が20分くらいから破壊と再生を繰り返す。

### AutoHalley
25分くらい経過すると惑星が衝突してゲームが終了する。

### Crimest System

#### 無印
yahhoによって作られたものすごく栗目な星系。おもい。
#### 1.1
修正版。

#### 1.2.2
さらに修正版。

#### 1.2.3 ZakuroTweak
ざくろが勝手にいじった版。

### ded-800
Metal惑星(以後dedster)が1つだけの星系。

#### ded-802
ded-800に着陸できない溶岩惑星を軌道周回させたもの。

#### ded-803
ded-800に**異常に小さなガス惑星**を軌道周回させたもの。

### Ded Deed Deddeded
でっど・でぃーど・でっででっど。
**Red Moon Orchestra**の惑星バイオームをすべてMetalに置き換えたもの。Annihilaserが起動するMetal惑星は3つだけ。

### Edge of the World
単一惑星の星系としてスタートするが、ランダムなタイミングで小惑星(小さいとは言ってない)が増えていく星系。ただし、それらはHalley1個で動かない。

### Fire Vortex
大きな溶岩惑星の周辺にまったく同一の中程度の惑星が2つ周回する星系。
しばらくすると大きな溶岩惑星がもう1つ現れる。

### FloodCrime
ガス惑星2つと、それらの周辺を軌道周回するいくつかの惑星がある星系。
青いガス惑星側にある小さな海洋惑星にテレポーターを設置することで**お手軽籠城**ができてしまうため、NoSiege版の使用を推奨。

#### FloodCrime NoSiege
修正版。こちらが推奨版。

### Girly Orchestra
Girly StellarとRed Moon Orchestraを足して2で割ったような星系。
1つのガス惑星の周辺に8つの惑星が周回している。

### Girly Stellar
すべてのバイオームの小さな惑星を1つずつ、すべて配置した惑星系。
したがって**天体が9つある**ため、**Halleyが飛び交う**。おっかない。
天体の配置やHalley数、スタート惑星をいじくり回す修正を行っている。

#### v1.1

#### v1.2

#### Super Girly Stellar
大栗目。すべての惑星を破壊しても**2秒で復活する**。

### GreenApple
惑星サイズ1,300のガス惑星の周りに惑星サイズ1,300の熱帯惑星が周回する。
衛星ユニットの移動が非常にはやく、ガス惑星を踏み台にすればどこにでも一瞬で移動できるのが重要。

### Ham System
ナマンハム大元帥の星系。資源の多い1つの巨大惑星からなる。

### iga's brain
いがめた星系。
複数箇所に資源が凝集している惑星からスタートする。

### MegaApple Prime
One Man Moon Bandより古い惑星系。
ガス惑星が2つあり、その間が非常に離れている。

### MicroApple
**サイズ250の海洋付き惑星1つ。**

### Mighty Stellar
Annihilaserが使い放題。

### One Man Moon Band
惑星サイズ1,200の惑星1つ。1Kとか2Kみたいなもん。
とてもふるい。

### PetitApple
地形のある1Kのようなもの。

### Prison-Banana

### Red Moon MegaApple Twist
文字通り、Red Moon OrchestraとMegaApple PrimeとZakuro Twistを足して2.9くらいで割ったような星系。
Twisterと呼ばれるdedsterの周りをガス惑星が周回し、その周辺にさらに小さな惑星が周回している。

### Red Moon Orchestra
dedsterに全コマンダーが押し込まれ、そこから7つもある他の惑星に散らかっていく惑星系。
ガス惑星の周りに5つの惑星と2つのMetal惑星がある。派生が多い。

#### Red Moon Orchestra Extreme
5つの惑星に溶岩がある。

#### Red Moon Orchestra -p0keR's over-over-overkuso remix-
(なんだっけ...)

#### Red Moon Orchestra VIP
5つの惑星に海洋がある。

#### Red Moon Orchsetra
Red Moon Orch**se**tra。
Red Moon Orchestraに擬態した惑星系。表示がRed Moon Orchestraっぽいが、開始とともに惑星が6つ消滅し、ガス惑星とdedsterだけになる。結果WhiteAppleに似た星系になる。

#### RMO Prime 2019
資源配置を公式っぽくしたもの。

### Sorausing System
ソラウシング星系。ぶっちゃけそらうさ関係ないのに「そらうさ星」とかある。
大きなスタート惑星の周りに月が2つ。

### su-da-chi

### Super Girly Stellar
Girly Stellarの節で解説。

### SuperApple
惑星サイズ1,300の惑星1つ。

### Supercell
ガス惑星の周りに海があったりなかったりする熱帯惑星が5つ。開始直後だけ惑星系が渦を巻く。

### Terminal
**終点**。
サイズ500のSandbox惑星が1つだけ。

### The Crime
大きな溶岩惑星1つ。地形がない。

### The Practice
練習目的で作った一連の星系群。

### The Practice 500
### The Practice 600
### The Practice 900
### The Practice Maximin

### TOEI Animation
:crime:

### Wet Crime System
(なんだっけこれ)

### Z-Lab
ぞかす閣下によって作られた星系。

#### 2

### Zakurick Asteroid Paradice
小惑星いっぱい！ｗそれだけ。「Paradice」とついている星系は全部ネタ。

### Zakurick Jovian Paradice
ガス惑星いっぱい！ｗやば！ｗ

### Zakurick Saturnian Paradice
もっとガス惑星いっぱい！ｗやば！ｗ
(まともにプレイできそうな感じにした)

### Zakuro Blast
大栗目。
ランダムに惑星がAutoHalleyされていく。

### Zakuro Minimal
大栗目。
サイズ250の極小惑星をサイズ100の超極小惑星が周回する。
3秒でロードが終わる。

### Zakuro Twist
「Twister」という極小の無資源惑星を少し大きな惑星が周回する星系。

### Zaturn - Zuranus
ガス惑星2つで惑星多めの星系。
外側のZuranusからスタートし、内側のZaturnにはdedsterなどがある。

### Zax
公式星系のPaxに似せて作ったふるーてぃーな惑星系。

### Zedna
海洋惑星1つ。

### Zeptune Light
とてつもなく遠くに別の惑星がある惑星系。
行き来にとてつもなく時間がかかり、試合が泥沼になるためとてもdeprecated。

### Zeres
Zakuro Blastのようなもの。小惑星帯をスタート惑星が逆走する。

### Zeris
砂漠惑星1つ。

### Zoka-System
:zokahype:

### Zuranus Prime
ガス惑星1つに海洋系惑星2つ。
シンプルなつもり。