{
    "name": "The Practice 500",
    "planets": [
        {
            "name": "Practice Object 1",
            "mass": 5000,
            "position_x": 0,
            "position_y": 15800,
            "velocity_x": -177.8920135498047,
            "velocity_y": -0.000007775906851748005,
            "required_thrust_to_move": 3,
            "starting_planet": true,
            "respawn": false,
            "start_destroyed": false,
            "min_spawn_delay": 0,
            "max_spawn_delay": 0,
            "planet": {
                "seed": 423883200,
                "radius": 500,
                "heightRange": 0,
                "waterHeight": 0,
                "waterDepth": 100,
                "temperature": 0,
                "metalDensity": 100,
                "metalClusters": 100,
                "metalSpotLimit": -1,
                "biomeScale": 50,
                "biome": "sandbox",
                "symmetryType": "none",
                "symmetricalMetal": false,
                "symmetricalStarts": false,
                "numArmies": 3,
                "landingZonesPerArmy": 0,
                "landingZoneSize": 0
            }
        }
    ]
}